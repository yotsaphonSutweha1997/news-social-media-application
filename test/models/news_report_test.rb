require 'test_helper'

class NewsReportTest < ActiveSupport::TestCase

  
  test "should save the report" do 
    user = User.new(username: "Yo", email: "yo@gmail.com", password: "asd45rty")
    user.save

    profile = Profile.new(fname: "yotsaphons", sname: "sutweha", bio: "i am a news writer", role: "news writer", user: user)

    report = NewsReport.new(title: "Football", category: "Sport", content: "Football is also known as soccer", profile: profile)
    assert report.save
  end


end
